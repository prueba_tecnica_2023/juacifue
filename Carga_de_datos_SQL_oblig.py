# -*- coding: utf-8 -*-
"""
Created on Fri Jul 28 19:38:01 2023

@author: juacifue
"""
import pyodbc
import pandas as pd

# Establecer la conexión a la base de datos
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para crear la tabla (reemplaza los nombres y tipos de columna según tus necesidades)
sql_create_table = """
CREATE TABLE Auditoriapdb.sucursales.prueba (
    radicado BIGINT,
    num_documento BIGINT,
    cod_segm_tasa VARCHAR(100),
    cod_subsegm_tasa INT,
    cal_interna_tasa VARCHAR(100),
    id_producto VARCHAR(100),
    tipo_id_producto VARCHAR(100),
    valor_inicial FLOAT,
    fecha_desembolso DATETIME,
    plazo FLOAT,
    cod_periodicidad DECIMAL(5,0),
    periodicidad VARCHAR(100),
    saldo_deuda FLOAT,
    modalidad VARCHAR(100),
    tipo_plazo VARCHAR(100)
)
"""

try:
    # Ejecutar el comando SQL para crear la tabla
    cursor.execute(sql_create_table)

    # Cerrar el cursor (no cerramos la conexión aquí para usarla en la carga de datos)
    cursor.close()

    print("La tabla se ha creado exitosamente.")
except pyodbc.Error as ex:
    # Si ocurre un error al crear la tabla, mostramos un mensaje de error
    print("Error al crear la tabla:", ex)

# Establecer la conexión a la base de datos nuevamente para cargar los datos
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Leer el archivo Excel en un DataFrame de pandas
archivo_excel = r'C:\Users\juacifue\Downloads\Obligaciones_clientes.xlsx'
hoja_excel = 'Obligaciones_clientes'
datos_df = pd.read_excel(archivo_excel, sheet_name=hoja_excel)

# Convertir la columna 'plazo' a un tipo numérico que admita nulos y reemplazar NaN por None
datos_df['plazo'] = pd.to_numeric(datos_df['plazo'], errors='coerce')

# Filtrar el DataFrame para eliminar las filas con valores None en la columna 'plazo'
datos_df = datos_df.dropna(subset=['plazo'])

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Iterar sobre las filas del DataFrame y cargar los datos en la tabla
for index, row in datos_df.iterrows():
    sql_insert = f"""
    INSERT INTO Auditoriapdb.sucursales.prueba (radicado, num_documento, cod_segm_tasa, cod_subsegm_tasa,
                         cal_interna_tasa, id_producto, tipo_id_producto, valor_inicial, fecha_desembolso,
                         plazo, cod_periodicidad, periodicidad, saldo_deuda, modalidad, tipo_plazo)
    VALUES ({row['radicado']}, {row['num_documento']}, '{row['cod_segm_tasa']}', {row['cod_subsegm_tasa']}, '{row['cal_interna_tasa']}',
            '{row['id_producto']}', '{row['tipo_id_producto']}', {row['valor_inicial']}, '{row['fecha_desembolso']}',
            {row['plazo']}, {row['cod_periodicidad']}, '{row['periodicidad']}', {row['saldo_deuda']},
            '{row['modalidad']}', '{row['tipo_plazo']}')
    """
    cursor.execute(sql_insert)

# Cerrar el cursor y la conexión
cursor.close()
lz_cn.close()

print("Los datos se han cargado exitosamente en la tabla.")

#####
####TASAS
#####
# Establecer la conexión a la base de datos
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para crear la tabla (reemplaza los nombres y tipos de columna según tus necesidades)
sql_create_table = """
CREATE TABLE Auditoriapdb.sucursales.tasas_clientes (
    cod_segmento VARCHAR(100),
    segmento VARCHAR(100),
    cod_subsegmento VARCHAR(100),
    calificacion_riesgos VARCHAR(100),
    tasa_cartera FLOAT,
    tasa_operacion_especifica FLOAT,
    tasa_hipotecario FLOAT,
    tasa_leasing FLOAT,
    tasa_sufi FLOAT,
    tasa_factoring FLOAT,
    tasa_tarjeta FLOAT
)
"""

try:
    # Ejecutar el comando SQL para crear la tabla
    cursor.execute(sql_create_table)

    # Cerrar el cursor (no cerramos la conexión aquí para usarla en la carga de datos)
    cursor.close()

    print("La tabla se ha creado exitosamente.")
except pyodbc.Error as ex:
    # Si ocurre un error al crear la tabla, mostramos un mensaje de error
    print("Error al crear la tabla:", ex)

# Establecer la conexión a la base de datos nuevamente para cargar los datos
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Leer el archivo Excel en un DataFrame de pandas
archivo_excel = r'C:\Users\juacifue\Downloads\tasas_productos.xlsx'
hoja_excel = 'Tasas'
datos_df = pd.read_excel(archivo_excel, sheet_name=hoja_excel)

# Convertir las columnas de tasas a tipos numéricos que admitan nulos y reemplazar NaN por None
tasas_columns = ['tasa_cartera', 'tasa_operacion_especifica', 'tasa_hipotecario', 'tasa_leasing',
                 'tasa_sufi', 'tasa_factoring', 'tasa_tarjeta']
for column in tasas_columns:
    datos_df[column] = pd.to_numeric(datos_df[column], errors='coerce')
    datos_df[column] = datos_df[column].apply(lambda x: None if pd.isna(x) else x)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Iterar sobre las filas del DataFrame y cargar los datos en la tabla
for index, row in datos_df.iterrows():
    sql_insert = f"""
    INSERT INTO Auditoriapdb.sucursales.tasas_clientes (cod_segmento, segmento, cod_subsegmento, calificacion_riesgos,
                         tasa_cartera, tasa_operacion_especifica, tasa_hipotecario, tasa_leasing, tasa_sufi,
                         tasa_factoring, tasa_tarjeta)
    VALUES ('{row['cod_segmento']}', '{row['segmento']}', '{row['cod_subsegmento']}', '{row['calificacion_riesgos']}',
            {row['tasa_cartera']}, {row['tasa_operacion_especifica']}, {row['tasa_hipotecario']},
            {row['tasa_leasing']}, {row['tasa_sufi']}, {row['tasa_factoring']}, {row['tasa_tarjeta']})
    """
    cursor.execute(sql_insert)

# Cerrar el cursor y la conexión
cursor.close()
lz_cn.close()

print("Los datos se han cargado exitosamente en la tabla.")


####

# Establecer la conexión a la base de datos nuevamente
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para crear la nueva tabla con la estructura adecuada
sql_create_nueva_tabla = """
CREATE TABLE Auditoriapdb.sucursales.nueva_tabla (
    radicado BIGINT,
    num_documento BIGINT,
    cod_segm_tasa VARCHAR(100),
    cod_subsegm_tasa INT,
    cal_interna_tasa VARCHAR(100),
    id_producto VARCHAR(100),
    tipo_id_producto VARCHAR(100),
    valor_inicial FLOAT,
    fecha_desembolso DATETIME,
    plazo FLOAT,
    cod_periodicidad DECIMAL(5,0),
    periodicidad VARCHAR(100),
    saldo_deuda FLOAT,
    modalidad VARCHAR(100),
    tipo_plazo VARCHAR(100),
    tasa_cartera FLOAT,
    tasa_operacion_especifica FLOAT,
    tasa_hipotecario FLOAT,
    tasa_leasing FLOAT,
    tasa_sufi FLOAT,
    tasa_factoring FLOAT,
    tasa_tarjeta FLOAT
);
"""

try:
    # Ejecutar el comando SQL para crear la nueva tabla
    cursor.execute(sql_create_nueva_tabla)

    # Cerrar el cursor
    cursor.close()

    print("La nueva tabla se ha creado exitosamente.")
except pyodbc.Error as ex:
    # Si ocurre un error al crear la tabla, mostramos un mensaje de error
    print("Error al crear la nueva tabla:", ex)

# Establecer la conexión a la base de datos nuevamente para cargar los datos cruzados
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para cargar los datos cruzados en la nueva tabla
sql_insert_data = """
INSERT INTO Auditoriapdb.sucursales.nueva_tabla (
    radicado,
    num_documento,
    cod_segm_tasa,
    cod_subsegm_tasa,
    cal_interna_tasa,
    id_producto,
    tipo_id_producto,
    valor_inicial,
    fecha_desembolso,
    plazo,
    cod_periodicidad,
    periodicidad,
    saldo_deuda,
    modalidad,
    tipo_plazo,
    tasa_cartera,
    tasa_operacion_especifica,
    tasa_hipotecario,
    tasa_leasing,
    tasa_sufi,
    tasa_factoring,
    tasa_tarjeta
)
SELECT p.*,
       t.tasa_cartera,
       t.tasa_operacion_especifica,
       t.tasa_hipotecario,
       t.tasa_leasing,
       t.tasa_sufi,
       t.tasa_factoring,
       t.tasa_tarjeta
FROM Auditoriapdb.sucursales.prueba p
LEFT JOIN Auditoriapdb.sucursales.tasas_clientes t
ON p.cod_segm_tasa = t.cod_segmento
AND p.cod_subsegm_tasa = t.cod_subsegmento
AND p.cal_interna_tasa = t.calificacion_riesgos;
"""

try:
    # Ejecutar el comando SQL para cargar los datos cruzados en la nueva tabla
    cursor.execute(sql_insert_data)

    # Cerrar el cursor y la conexión
    cursor.close()
    lz_cn.close()

    print("Los datos se han cargado exitosamente en la nueva tabla.")
except pyodbc.Error as ex:
    # Si ocurre un error al cargar los datos, mostramos un mensaje de error
    print("Error al cargar los datos en la nueva tabla:", ex)

##### AGREGAR COLUMNA





 
# Establecer la conexión a la base de datos nuevamente
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para calcular y actualizar las tasas efectivas en la tabla nueva_tabla
sql_update_tasas_efectivas = """
UPDATE Auditoriapdb.sucursales.nueva_tabla
SET tasa_efectiva_cartera = (((POWER(1 + tasa_cartera, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_operacion = (((POWER(1 + tasa_operacion_especifica, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_hipotecaria = (((POWER(1 + tasa_hipotecario, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_leasing = (((POWER(1 + tasa_leasing, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_sufi = (((POWER(1 + tasa_sufi, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_factoring = (((POWER(1 + tasa_factoring, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100,
    tasa_efectiva_tarjeta = (((POWER(1 + tasa_tarjeta, 1 / (12 / cod_periodicidad))) - 1) * (12 / cod_periodicidad)) * 100;
"""

try:
    # Ejecutar el comando SQL para calcular y actualizar las tasas efectivas
    cursor.execute(sql_update_tasas_efectivas)

    # Cerrar el cursor y la conexión
    cursor.close()
    lz_cn.close()

    print("Se ha calculado y actualizado correctamente la columna de tasas efectivas en la tabla nueva_tabla.")
except pyodbc.Error as ex:
    # Si ocurre un error al ejecutar los comandos, mostramos un mensaje de error
    print("Error al calcular y actualizar la columna de tasas efectivas:", ex)
#########
###Valor Final
########
# Establecer la conexión a la base de datos nuevamente
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para añadir la nueva columna "valor_final" a la tabla "nueva_tabla"
sql_add_column = """
ALTER TABLE Auditoriapdb.sucursales.nueva_tabla
ADD valor_final FLOAT;
"""

try:
    # Ejecutar el comando SQL para añadir la nueva columna
    cursor.execute(sql_add_column)

    # Cerrar el cursor
    cursor.close()

    print("La columna 'valor_final' se ha añadido exitosamente a la tabla 'nueva_tabla'.")
except pyodbc.Error as ex:
    # Si ocurre un error al ejecutar el comando, mostramos un mensaje de error
    print("Error al añadir la columna 'valor_final':", ex)

# Establecer la conexión a la base de datos nuevamente para calcular y actualizar la columna "valor_final"
lz_cn = pyodbc.connect("DSN=DMIN", autocommit=True)

# Crear un objeto cursor para ejecutar comandos SQL
cursor = lz_cn.cursor()

# Comando SQL para calcular y actualizar la columna "valor_final" en la tabla "nueva_tabla"
sql_update_valor_final = """
UPDATE Auditoriapdb.sucursales.nueva_tabla
SET valor_final = valor_inicial * tasa_efectiva_cartera;
"""

try:
    # Ejecutar el comando SQL para calcular y actualizar la columna "valor_final"
    cursor.execute(sql_update_valor_final)

    # Cerrar el cursor y la conexión
    cursor.close()
    lz_cn.close()

    print("La columna 'valor_final' se ha calculado y actualizado correctamente en la tabla 'nueva_tabla'.")
except pyodbc.Error as ex:
    # Si ocurre un error al ejecutar los comandos, mostramos un mensaje de error
    print("Error al calcular y actualizar la columna 'valor_final':", ex)


#### UMBRAL
# Calcular la suma del valor_final por cliente y obtener el promedio
sql_calculate_sum = """
DECLARE @promedio_suma_valor_final FLOAT;

SELECT num_documento, SUM(valor_final) AS suma_valor_final
INTO #TempTablaSumaValor
FROM Auditoriapdb.sucursales.nueva_tabla
GROUP BY num_documento;

-- Obtener el promedio de la suma del valor_final por cliente
SELECT @promedio_suma_valor_final = AVG(suma_valor_final) FROM #TempTablaSumaValor;

-- Filtrar los clientes cuya suma es superior al promedio y almacenar los resultados en la tabla 'valor_final'
SELECT t1.*
INTO Auditoriapdb.sucursales.valor_final
FROM Auditoriapdb.sucursales.nueva_tabla t1
INNER JOIN #TempTablaSumaValor t2 ON t1.num_documento = t2.num_documento
WHERE t2.suma_valor_final > @promedio_suma_valor_final;

-- Eliminar la tabla temporal #TempTablaSumaValor
DROP TABLE #TempTablaSumaValor;
"""

# Ejecutar la consulta para calcular la suma, obtener el promedio, filtrar y almacenar los resultados en la tabla 'valor_final'
cursor.execute(sql_calculate_sum)

#####
#### CIERRE DE CONEXIÓN
#####

# Cerrar el cursor y la conexión
cursor.close()
lz_cn.close()

print("El proceso se ha completado correctamente.")


#####
## QUERYS
####

###1) SELECT * FROM CLIENTE
###2) SELECT * FROM CLIENTE WHERE Region = 'Centro';

###3)SELECT c.*
#FROM CLIENTE c
#INNER JOIN CUENTAS cu ON c.Cedula = cu.Cedula_cliente
#WHERE cu.Estado = 1
#GROUP BY c.Cedula, c.Nombre, c.Region, c.Edad
#HAVING COUNT(cu.Num_cuenta) > 3;

###4)SELECT c.Nombre
#FROM CLIENTE c
#INNER JOIN CLAVE_DINAMICA cd ON c.Cedula = cd.cedula_cliente;


###5) SELECT c.*
#FROM CLIENTE c
#LEFT JOIN CLAVE_DINAMICA cd ON c.Cedula = cd.cedula_cliente
#WHERE cd.cedula_cliente IS NULL;

###6)SELECT c.Region, SUM(cu.Saldo) AS Saldo_Total
#FROM CLIENTE c
#INNER JOIN CUENTAS cu ON c.Cedula = cu.Cedula_cliente
#GROUP BY c.Region;

###7)SELECT c.Nombre, SUM(cu.Saldo) AS Saldo_Total
#FROM CLIENTE c
#INNER JOIN CUENTAS cu ON c.Cedula = cu.Cedula_cliente
#INNER JOIN CLAVE_DINAMICA cd ON c.Cedula = cd.cedula_cliente
#WHERE cu.Estado = 1 AND cu.Fecha_apertura >= '20180501' AND cu.Fecha_apertura <= '20180531'
#GROUP BY c.Nombre;


